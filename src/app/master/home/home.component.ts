import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, RequiredValidator } from '@angular/forms';

import { ResultComponent} from '../result/result.component';
import {  CrudService} from '../../service/crud.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private result: CrudService,private router: Router) { }

  ngOnInit(): void {
  }
  resultForm = new FormGroup({
    en_no: new FormControl(''),
    studName: new FormControl(''),
    sub1: new FormControl(''),
    sub2: new FormControl(''),
    sub3: new FormControl(''),
  });

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.resultForm.value);
    this.router.navigate(['/result']);
    this.result.resultData(this.resultForm.value);
    
  }
}
