import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './master/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResultComponent } from './master/result/result.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ResultComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
