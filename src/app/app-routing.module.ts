import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './master/home/home.component';
import { ResultComponent } from './master/result/result.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },  
  {
    path:"result",
    component:ResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
